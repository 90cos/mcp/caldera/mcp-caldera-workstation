
# linux host below
# vagrant destroy -f && rm -rf .vagrant && vagrant up
# windows host below
# vagrant destroy -f; del -Recurse .vagrant; vagrant up
Vagrant.configure("2") do |config|
    config.vm.define "mcp-caldera-dev" do |node|
        node.vm.box = "generic/ubuntu2004"
        node.vm.synced_folder '.', "/vagrant", disabled: true  # enabled by default, force disabled
        node.vm.hostname = "mcp-caldera-dev"
        node.vm.network "private_network", ip: "192.168.60.81"
        node.vm.provider "virtualbox" do |vb|
            vb.cpus = 4
            vb.memory = 8192
            vb.gui = false
        end
        node.vm.provision "shell", inline: <<-PerformSetup1
            apt-get update -yq
        PerformSetup1
        node.vm.provision "shell", inline: <<-PerformSetup2
            DEBIAN_FRONTEND=noninteractive apt-get -yq upgrade
        PerformSetup2
        node.vm.provision "shell", inline: <<-PerformSetup4
            echo "**** installing dev tools ****"
            apt-get install -yq openssh-server net-tools git build-essential cmake python3-wheel python3-dev python3-pip docker.io
            apt-get install -yq libxi6 libxtst6 x11-apps
            sed -i 's/#X11UseLocalhost yes/X11UseLocalhost no/g' /etc/ssh/sshd_config
            usermod -aG docker vagrant
            echo "**** installing ide of choice ****"
            echo snap install pycharm-community --classic
            echo "**** preparing dev env ****"
        PerformSetup4
        node.vm.provision "file", source: "context/deploy-dev-env.sh", destination: "/home/vagrant/Desktop/deploy-dev-env.sh"
        node.vm.provision "file", source: "context/setup-conda.sh", destination: "/home/vagrant/Desktop/setup-conda.sh"
        node.vm.provision "file", source: "context/docker-compose.yml", destination: "/home/vagrant/Desktop/docker-compose.yml"
        node.vm.provision "file", source: "context/restart_brain.sh", destination: "/home/vagrant/Desktop/restart_brain.sh"
        node.vm.provision "file", source: "context/wrap-pip-install.sh", destination: "/home/vagrant/Desktop/wrap-pip-install.sh"
        node.vm.provision "shell", inline: <<-ExecPerm
            chmod +x /home/vagrant/Desktop/*.sh
        ExecPerm
        node.vm.provision "shell", inline: <<-PerformSetup5
            cd /home/vagrant/Desktop
            su -c './setup-conda.sh' vagrant
            sleep 60
        PerformSetup5
        node.vm.provision "shell", inline: <<-PerformSetup6
            cd /home/vagrant/Desktop
            echo ** Obnoxious way to get the dev environment installed in a conda base environment **
            su -c 'bash -i ./deploy-dev-env.sh' vagrant
            sleep 60
        PerformSetup6
    end
end
