# Known Limitations

Caldera seems to work best with MCP plugins on **python 3.9**

Python 3.9 is available in Alpine Linux 3.15, but not in an Ubuntu LTS release.

Therefore, the Dev VM will be setup to use miniconda's Python 3.9 rather than the packaged version of python.

# Setup
## Create VM on Windows
```shell
vagrant destroy -f; del -Recurse .vagrant; vagrant up
```


## Create VM on Linux

```shell
vagrant destroy -f && rm -rf .vagrant && vagrant up
```

# Develop Caldera against PEMR/MCP

Right now there is some shenanigans to get it working with python3.9.

3.9 is the most comfortable to develop against with the least problems