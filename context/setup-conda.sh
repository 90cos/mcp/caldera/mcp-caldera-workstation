#!/usr/bin/env bash

echo "CALDERA works best with python 3.9"
curl -o /tmp/Miniconda3-py39.sh https://repo.anaconda.com/miniconda/Miniconda3-py39_4.12.0-Linux-x86_64.sh
chmod +x /tmp/Miniconda3-py39.sh
/tmp/Miniconda3-py39.sh -b
~/miniconda3/bin/conda init bash


