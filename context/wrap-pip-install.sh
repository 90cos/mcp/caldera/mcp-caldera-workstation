#!/usr/bin/env bash


pip3 install ramrodbrain --extra-index-url https://gitlab.com/api/v4/projects/25582434/packages/pypi/simple
pip3 install plugalone   --index-url       https://gitlab.com/api/v4/projects/37969266/packages/pypi/simple


if [ -f "requirements.txt" ]; then
    pip3 install -r requirements.txt
fi
if [ -f "requirements-new.txt" ]; then
    pip3 install -r requirements-new.txt
fi


