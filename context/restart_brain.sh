#!/usr/bin/env bash

docker service scale calderadev_brain=0
docker service scale calderadev_sockets=0
docker service scale calderadev_web=0
sleep 10
docker service scale calderadev_brain=1
sleep 10
docker service scale calderadev_sockets=1
sleep 10
docker service scale calderadev_web=1

