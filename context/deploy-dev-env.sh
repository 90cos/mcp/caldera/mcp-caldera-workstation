#!/usr/bin/env bash
cd ~/Desktop/
echo "================================================"
echo "= Initial Setup - This script should run once  ="
if [[ -z "${UPSTREAM40_BRANCH}" ]]; then
CHOSEN_UPSTREAM_BRANCH="main"
echo "=               - UPSTREAM40_BRANCH isn't set  ="
echo "=               - getting the main branch      ="
else
CHOSEN_UPSTREAM_BRANCH="${UPSTREAM40_BRANCH}"
echo "=               - UPSTREAM40_BRANCH set to     ="
echo "=               -   ${CHOSEN_UPSTREAM_BRANCH}   "
echo "=               - getting that branch          ="
fi
echo "=               - of modded upstream caldera   ="
echo "================================================"
echo "= Initial Setup - This script should run once  ="
if [[ -z "${INTEROP_BRANCH}" ]]; then
CHOSEN_INTEROP_BRANCH="master"
echo "=               - INTEROP_BRANCH isn't set  ="
echo "=               - getting the main branch      ="
else
CHOSEN_INTEROP_BRANCH="${INTEROP_BRANCH}"
echo "=               - INTEROP_BRANCH set to        ="
echo "=               -   ${CHOSEN_INTEROP_BRANCH}    "
echo "=               - getting that branch          ="
fi
echo "=               - of caldera / MCP interop     ="
echo "================================================"
mkdir -p ~/PycharmProjects
git clone --branch=${CHOSEN_UPSTREAM_BRANCH} https://gitlab.com/90cos/mcp/caldera/upstream-40.git ~/PycharmProjects/upstream-40
git clone --branch=${CHOSEN_INTEROP_BRANCH} https://gitlab.com/90cos/mcp/caldera/mcp-interop.git ~/PycharmProjects/mcp-interop


cd ~/Desktop
docker swarm init --advertise-addr eth0
docker stack deploy --compose-file docker-compose.yml calderadev

